#!/usr/bin/python3
# web-master v1.0  (c) 16.7.2024 by Andreas Ley  (u) 16.7.2024
# Web-master slave backend

import argparse
import yaml
import jinja2
from jinja2 import Environment, FileSystemLoader
import urllib.parse

import sys
import os
import re
from collections import defaultdict

import json

base = '/usr/lib/web-master'
templates = base + '/templates'


def warn(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='web-master slave backend')
	parser.add_argument('-v', '--verbose', action='store_true', help='verbose mode')
	parser.add_argument('-n', '--dry-run', action='store_true', help='dry run')
	parser.add_argument('-D', '--debug', action='store_true', help='debug mode')
	parser.add_argument('arguments', nargs='*', help='argument(s)')
	args = parser.parse_args()

	env = Environment(loader=FileSystemLoader(templates))
	#env.filters['regex_replace'] = lambda value, pattern, replacement: re.sub(pattern, replacement, value)
	env.filters['date'] = lambda date: date.strftime('%d.%m.%Y')
	env.filters['exists'] = lambda path: os.path.exists(path)
	env.filters['resplit'] = lambda text, pattern: re.split(pattern, text)
	#env.filters['path_safe'] = lambda path: path_safe(path)
